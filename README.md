# Information Security

**6 Ways to Protect Your Personal Information Online**

1. Create **strong** passwords
2. Use free Wi-Fi with caution
3. Don't use one **username** for many social media accounts
4. Watch out for links and attachments
5. Don't **overshare** on social media

